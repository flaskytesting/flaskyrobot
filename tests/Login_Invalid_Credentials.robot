*** Settings ***
Documentation   Tests for Demp app site - Invalid Data In Registration.
Suite Setup     Open Test Site
Test Template   Login With Invalid Credentials Should Fail
Suite Teardown  Close The Browser
Resource        shared_keywords.robot

*** Test Cases ***      USERNAME                    PASSWORD
Existing Username       Invalid                     ${PASSWORD}
Empty Username          ${EMPTY}                    ${PASSWORD}
Empty Password          ${USERNAME}                 ${EMPTY}

*** Keywords ***
Login With Invalid Credentials Should Fail
    [Documentation]    Login with invalid credentails should fail the test
    [Arguments]    ${username}    ${password}
    Go To Login Page
    Fill Login Form With Data Fields      ${username}    ${password}
    Submit Form
    Login Should Have Failed        ${username}    ${password}