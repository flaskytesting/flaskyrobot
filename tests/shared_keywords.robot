*** Settings ***
Documentation     Shared functionality
Library           SeleniumLibrary
Library           String
Library           BuiltIn
Library           DateTime
Library           OperatingSystem
Resource          variables.robot

*** Keywords ***
Initial Setup
    Open Test Site
    Successful Registration Through Web Portal
    
Open Test Site
    Visit Test Site

Visit Test Site
    Open Browser    ${TEST-SITE}    ${BROWSER}
    Maximize Browser Window 
    Page Should Contain     Demo app

Close The Browser
    Close Browser
    Create File    ${STATUSFILE}    ${SUITE_STATUS}

Successful Registration Through Web Portal
    Go To Registration Page
    ${currenttime}=                             Generate Unique Credentials
    Fill Registration Form With Data Fields     ${USERNAME}${currenttime}   ${PASSWORD}     ${FIRSTNAME}    ${FAMILYNAME}   ${PHONE}
    Submit Form
    Verify Successful Registration
    set global variable                         ${currenttime}


Registration With Existing Credential
     Go To Registration Page
     Fill Registration Form With Data Fields    ${USERNAME}${currenttime}   ${PASSWORD}     ${FIRSTNAME}    ${FAMILYNAME}   ${PHONE}
     Submit Form
     Verify Unsuccessful Registration           ${USERNAME}${currenttime}

Go To Registration Page
    Wait Until Page Contains Element            xpath=//a[@href="/register"]
    Click Element                               xpath=//a[@href="/register"]
    Element Should Contain                      CSS:section > header > h1       Register

Generate Unique Credentials
    ${currenttime}=                             Get Current Date
    [return]                                    ${currenttime}
    

Fill Registration Form With Data Fields
    [Arguments]    ${username}   ${password}     ${firstname}    ${familyname}   ${phone}
                   
    Wait Until Page Contains Element            xpath=//input[@name="username"]
    Input Text                                  xpath=//input[@name="username"]       ${username}
    Wait Until Page Contains Element            xpath=//input[@name="password"]
    Input Text                                  xpath=//input[@name="password"]       ${password}
    Wait Until Page Contains Element            xpath=//input[@name="firstname"]
    Input Text                                  xpath=//input[@name="firstname"]      ${firstname}
    Wait Until Page Contains Element            xpath=//input[@name="lastname"]
    Input Text                                  xpath=//input[@name="lastname"]       ${familyname}
    Wait Until Page Contains Element            xpath=//input[@name="phone"]
    Input Text                                  xpath=//input[@name="phone"]          ${phone}

Submit Form
    Wait Until Page Contains Element            xpath=//input[@type="submit"]
    Click Element                               xpath=//input[@type="submit"]

Verify Successful Registration
    Element Should Contain                      CSS:section > header > h1       Log In

Registration Should Have Failed
    Element Should Not Contain                  CSS:section > header > h1       Log In

Verify Unsuccessful Registration
    [Arguments]    ${existing-user}
    Element Should Contain                      xpath=//div[@class="flash"]       User ${existing-user} is already registered.

Successful Login Through Web Portal
    Go To Login Page
    Fill Login Form With Data Fields     ${USERNAME}${currenttime}   ${PASSWORD}
    Submit Form
    Verify Successful Login

Go To Login Page
    Wait Until Page Contains Element            xpath=//a[@href="/login"]
    Click Element                               xpath=//a[@href="/login"]
    Element Should Contain                      CSS:section > header > h1       Log In

Fill Login Form With Data Fields
    [Arguments]    ${username}   ${password}
                   
    Wait Until Page Contains Element            xpath=//input[@name="username"]
    Input Text                                  xpath=//input[@name="username"]       ${username}
    Wait Until Page Contains Element            xpath=//input[@name="password"]
    Input Text                                  xpath=//input[@name="password"]       ${password}

Verify Successful Login
    Element Should Contain                      CSS:section > header > h1             User Information
    Element Should Contain                      xpath=//td[@id="username"]            ${USERNAME}${currenttime}
    Element Should Contain                      xpath=//td[@id="firstname"]           ${FIRSTNAME}
    Element Should Contain                      xpath=//td[@id="lastname"]            ${FAMILYNAME}
    Element Should Contain                      xpath=//td[@id="phone"]               ${PHONE}   

Login Should Have Failed
    [Arguments]    ${username}   ${password}
    Run Keyword If      '${username}' == 'Invalid'      Page Should Contain     Login Failure         
    Element Should Not Contain                          CSS:section > header > h1       User Information

