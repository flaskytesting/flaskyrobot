*** Settings ***
Documentation     Variables - A resource file with reusable variables.

*** Variables ***
${TEST-SITE}        http://localhost:8080/
${STATUSFILE}       /reports/statusFile.txt
${USERNAME}         username
${PASSWORD}         password
${FIRSTNAME}        firstname
${FAMILYNAME}       familyname
${PHONE}            987654321
${BROWSER}          chrome

