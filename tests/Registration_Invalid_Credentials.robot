*** Settings ***
Documentation       Tests for Demp app site - Invalid Data In Registration.
Suite Setup         Initial Setup
Test Template       Register With Invalid Credentials Should Fail
Suite Teardown      Close The Browser
Resource            shared_keywords.robot

*** Test Cases ***      USERNAME                    PASSWORD        FIRSTNAME       FAMILYNAME      PHONE
Existing Username       ${USERNAME}${currenttime}   ${PASSWORD}     ${FIRSTNAME}    ${FAMILYNAME}   ${PHONE}
Empty Username          ${EMPTY}                    ${PASSWORD}     ${FIRSTNAME}    ${FAMILYNAME}   ${PHONE}
Empty Password          ${USERNAME}                 ${EMPTY}        ${FIRSTNAME}    ${FAMILYNAME}   ${PHONE}
Empty Firstname         ${USERNAME}                 ${PASSWORD}     ${EMPTY}        ${FAMILYNAME}   ${PHONE}
Empty Familyname        ${USERNAME}                 ${PASSWORD}     ${FIRSTNAME}    ${EMPTY}        ${PHONE}
Empty Phone             ${USERNAME}                 ${PASSWORD}     ${FIRSTNAME}    ${FAMILYNAME}   ${EMPTY}

*** Keywords ***
Register With Invalid Credentials Should Fail
    [Documentation]    Registartion with invalid credentails should fail the test
    [Arguments]    ${username}    ${password}    ${firstname}   ${familyname}    ${phone}
    Go To Registration Page
    Fill Registration Form With Data Fields      ${username}    ${password}    ${firstname}   ${familyname}    ${phone}
    Submit Form
    Registration Should Have Failed