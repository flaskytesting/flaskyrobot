*** Settings ***
Documentation   Tests for Demp app site - Registartion with valid values.        
Suite Setup     Open Test Site
Suite Teardown  Close The Browser
Resource        shared_keywords.robot

*** Test Cases ***
User Is Able To Register Through Web Portal
    [Documentation]    New user registration with valid Username, Password, First name, Family Name and Phone number
    [Tags]             Registartion     Userinfo
    Successful Registration Through Web Portal
    
User Is Able To View Valid User Information 
    [Documentation]    Existing user is able to view user information with valid credentials
    [Tags]             Login     Userinfo
    Successful Registration Through Web Portal
    Successful Login Through Web Portal

*** Keywords ***

