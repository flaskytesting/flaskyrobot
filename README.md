# Customer: Demo app
## Test Information
```bash
Test Type:              UI testing

Test Coverage:          1. User can register through web portal
                        2. user can review his/her own user information from the main view
                        3. USer can not register if the form is incomplete
                        4. User can not register using existing username
                        5. User can not login with invalid credentials
Test Execution:         Clone the project repository flaskyrobot into a <directory>
                        cd <directory>
                        run command 
                        robot run <test repository>/<test suite>.robot - for specific test
                            e.g robot run tests/User_Registration_Login.robot
                        robot run <test repository> - for all test
                            e.g robot run tests/

Automation Test tool:   Robot framework with Selenium

Installation:           Install robot framework http://robotframework.org/
                        pip install --upgrade robotframework-seleniumlibrary
                        pip install robotframework-lint - for static analysis

Test Results:           
    1. Results can be found in log.html and report.html files in flaskyrobot directory
```
